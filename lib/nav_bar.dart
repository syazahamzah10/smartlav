import 'package:flutter/material.dart';
import 'package:smartlav/battery.dart';
import 'package:smartlav/homepage.dart';
import 'package:smartlav/profile.dart';
import 'package:smartlav/steps.dart';

class BottomNavigation extends StatefulWidget {
  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {

  int _currentIndex = 0;

  final List<Widget> _pages = [
    Homepage(),
    Steps(),
    Battery(),
    Profile()
  ];

//   void onTabTapped(int index) {
//    setState(() {
//      _currentIndex = index;
//    });
//  }

  @override
  Widget build(BuildContext context) {
      return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.teal,
        fixedColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        onTap: (index){
          setState(() {
            _currentIndex = index;
          });
        },
        currentIndex: _currentIndex,
        items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Home'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.directions_run),
          title: Text('Steps'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.battery_std),
          title: Text('Powerbank'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          title: Text('Profile'),
        ),
      ]),
      body: _pages[_currentIndex],
    );
  }
}

