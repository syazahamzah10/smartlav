import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:smartlav/chart.dart';
import 'package:smartlav/routes.dart';

class Steps extends StatefulWidget {
  @override
  _StepsState createState() => _StepsState();
}

class _StepsState extends State<Steps> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Steps'),
          centerTitle: true,
          backgroundColor: Colors.teal,
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 5.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      CircularPercentIndicator(
                        progressColor: Colors.blueAccent,
                        percent: 0.5,
                        animation: true,
                        radius: 80.0,
                        lineWidth: 10.0,
                        circularStrokeCap: CircularStrokeCap.round,
                        center: Text('7KM'),
                      ),
                      Text('Distance'),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      CircularPercentIndicator(
                        progressColor: Colors.deepOrangeAccent,
                        percent: 0.2,
                        animation: true,
                        radius: 80.0,
                        lineWidth: 10.0,
                        circularStrokeCap: CircularStrokeCap.round,
                        center: Text('45MIN'),
                      ),
                      Text('Time')
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      CircularPercentIndicator(
                        progressColor: Colors.purpleAccent,
                        percent: 0.8,
                        animation: true,
                        radius: 80.0,
                        lineWidth: 10.0,
                        circularStrokeCap: CircularStrokeCap.round,
                        center: Text('120KCAL'),
                      ),
                      Text('Calories'),
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      CircularPercentIndicator(
                        progressColor: Colors.greenAccent,
                        percent: 0.3,
                        animation: true,
                        radius: 80.0,
                        lineWidth: 10.0,
                        circularStrokeCap: CircularStrokeCap.round,
                        center: Text('989'),
                      ),
                      Text('Steps')
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      CircularPercentIndicator(
                        progressColor: Colors.yellowAccent,
                        percent: 0.7,
                        animation: true,
                        radius: 80.0,
                        lineWidth: 10.0,
                        circularStrokeCap: CircularStrokeCap.round,
                        center: Text('7KM'),
                      ),
                      Text('Heartbeat'),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              Text("Trends"),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  child: LineChartSample2(),
                ),
              ),
              SizedBox(
                height: 1.0,
              ),
              Container(
                height: 50.0,
                width: 300.0,
                child: RaisedButton(
                    child: Text('START RUNNING'),
                    textColor: Colors.white,
                    color: Colors.teal,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Routes()),
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
