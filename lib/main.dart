import 'package:flutter/material.dart';
import 'package:smartlav/login.dart';
import 'package:smartlav/nav_bar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Login(),
      routes: {
        '/login': (context) => Login(),
        '/navigation': (context) => BottomNavigation(),
      },
    );
  }
}
