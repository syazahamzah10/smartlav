import 'package:flutter/material.dart';

class Battery extends StatefulWidget {
  @override
  _BatteryState createState() => _BatteryState();
}

class _BatteryState extends State<Battery> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Battery'),
        centerTitle: true,
        backgroundColor: Colors.teal,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Image.asset('images/battery.png'),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      ' BATTERY: ',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      '67%',
                      style: TextStyle(
                        color: Colors.greenAccent,
                        fontSize: 30,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text('Steps Required: 155')
              ],
            ),
          ),
          SizedBox(
                  height: 5,
                ),
          Container(
            width: 420.0,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: DataTable(columns: [
                    DataColumn(label: Text('No')),
                    DataColumn(label: Text('Steps')),
                    DataColumn(label: Text('Battery Charged')),
                  ], rows: [
                    DataRow(cells: [
                      DataCell(Text('1')),
                      DataCell(Text('75')),
                      DataCell(Text('5%-25%')),
                    ]),
                    DataRow(cells: [
                      DataCell(Text('2')),
                      DataCell(Text('125')),
                      DataCell(Text('26%-50%')),
                    ]),
                    DataRow(cells: [
                      DataCell(Text('3')),
                      DataCell(Text('345')),
                      DataCell(Text('51%-75%')),
                    ]),
                    DataRow(cells: [
                      DataCell(Text('4')),
                      DataCell(Text('500')),
                      DataCell(Text('76%-100%')),
                    ]),
                  ]),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
